<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::factory()->count(20)->create();

        $categories = Category::all()->shuffle();
        if (Category::count() === 0) {
            $categories = Category::factory()
                ->count(20)
                ->create()
                ->shuffle();
        }

        foreach ($products as $product) {
            $category_ids = $categories->splice(rand(Product::MIN_CATEGORIES_COUNT, Product::MAX_CATEGORIES_COUNT) - 1)
                ->pluck('id')
                ->toArray();
            $product->categories()->sync($category_ids);
        }


    }
}
