<?php

namespace App\Repositories;

use App\Contracts\Entities\CategoryEntityInterface;
use App\Contracts\Repositories\CategoryRepositoryInterface;
use App\Models\Category;
use App\Models\CategoryProduct;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function list(): Collection|array|LengthAwarePaginator
    {
        return Category::all();
    }

    /**
     * @inheritDoc
     */
    public function getById(string $id): CategoryEntityInterface
    {
        return Category::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function store(CategoryEntityInterface $entity): CategoryEntityInterface
    {
        $entity->save();

        return $entity;
    }

    /**
     * @inheritDoc
     */
    public function update(CategoryEntityInterface $entity): CategoryEntityInterface
    {
        $updatedEntity = $this->getById($entity->getId());

        $updatedEntity->setTitle($entity->getTitle());

        $updatedEntity->save();

        return $updatedEntity;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(string $id): void
    {
        $entity = $this->getById($id);

        $entity->delete();
    }

    /**
     * @inheritDoc
     */
    public function productCountsById(string $category_id): int
    {
        return CategoryProduct::whereCategoryId($category_id)->count();
    }
}
