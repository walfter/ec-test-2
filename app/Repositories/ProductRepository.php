<?php

namespace App\Repositories;

use App\Contracts\Entities\CategoryEntityInterface;
use App\Contracts\Entities\ProductEntityInterface;
use App\Contracts\Repositories\ProductRepositoryInterface;
use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function list(): Collection|LengthAwarePaginator|array
    {
        return Product::with('categories')->filtered()->get();
    }

    /**
     * @inheritDoc
     */
    public function getById(string $id): ProductEntityInterface
    {
        return Product::with('categories')->withTrashed()
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * @inheritDoc
     */
    public function store(ProductEntityInterface $entity): ProductEntityInterface
    {
        $product = Product::create($entity->toArray());
        $product->categories()->sync($entity->getCategoriesID());
        return $product;
    }

    /**
     * @inheritDoc
     */
    public function update(ProductEntityInterface $entity): ProductEntityInterface
    {
        $attributes = $entity->toArray();
        unset($attributes['id']);

        $attributes = array_filter($attributes);

        $updatedEntity = $this->getById($entity->getId());
        if (!empty($attributes)) {
            $updatedEntity->update($attributes);
            $categories_id = $entity->getCategoriesID();
            $updatedEntity->categories()->sync($categories_id);
        }

        return $updatedEntity;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(string $id): void
    {
        $entity = $this->getById($id);

        $entity->delete();
    }
}
