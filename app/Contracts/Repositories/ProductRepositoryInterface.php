<?php

namespace App\Contracts\Repositories;

use App\Contracts\Entities\ProductEntityInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface ProductRepositoryInterface
{
    /**
     * @return Collection|LengthAwarePaginator|ProductEntityInterface[]
     */
    public function list(): Collection|LengthAwarePaginator|array;

    /**
     * @param string $id
     *
     * @return ProductEntityInterface
     * @throws ModelNotFoundException
     */
    public function getById(string $id): ProductEntityInterface;

    /**
     * @param ProductEntityInterface $entity
     *
     * @return ProductEntityInterface
     */
    public function store(ProductEntityInterface $entity): ProductEntityInterface;

    /**
     * @param ProductEntityInterface $entity
     *
     * @return ProductEntityInterface
     * @throws ModelNotFoundException
     */
    public function update(ProductEntityInterface $entity): ProductEntityInterface;

    /**
     * @param string $id
     *
     * @return void
     * @throws ModelNotFoundException
     */
    public function deleteById(string $id): void;
}
