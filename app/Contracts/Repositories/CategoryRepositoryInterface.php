<?php

namespace App\Contracts\Repositories;

use App\Contracts\Entities\CategoryEntityInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface CategoryRepositoryInterface
{
    /**
     * @return Collection|CategoryEntityInterface[]|LengthAwarePaginator
     */
    public function list(): Collection|array|LengthAwarePaginator;

    /**
     * @param string $id
     *
     * @return CategoryEntityInterface
     * @throws ModelNotFoundException
     */
    public function getById(string $id): CategoryEntityInterface;

    /**
     * @param CategoryEntityInterface $entity
     *
     * @return CategoryEntityInterface
     */
    public function store(CategoryEntityInterface $entity): CategoryEntityInterface;

    /**
     * @param CategoryEntityInterface $entity
     *
     * @return CategoryEntityInterface
     * @throws ModelNotFoundException
     */
    public function update(CategoryEntityInterface $entity): CategoryEntityInterface;

    /**
     * @param string $id
     *
     * @return void
     */
    public function deleteById(string $id): void;

    /**
     * @param string $category_id
     *
     * @return int
     */
    public function productCountsById(string $category_id): int;
}
