<?php

namespace App\Contracts\Entities;

interface CategoryEntityInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self;

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self;
}
