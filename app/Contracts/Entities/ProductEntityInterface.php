<?php

namespace App\Contracts\Entities;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

interface ProductEntityInterface extends Arrayable
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return bool
     */
    public function isPublished(): bool;

    /**
     * @return bool
     */
    public function isDeleted(): bool;

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self;

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self;

    /**
     * @param float $price
     *
     * @return $this
     */
    public function setPrice(float $price): self;

    /**
     * @param bool $isPublished
     *
     * @return $this
     */
    public function setPublished(bool $isPublished = true): self;

    /**
     * @param bool $deleted
     *
     * @return $this
     */
    public function setDeleted(bool $deleted = true): self;

    /**
     * @return Collection|array
     */
    public function getCategories(): Collection|array;

    /**
     * @return array
     */
    public function getCategoriesID(): array;

    /**
     * @param CategoryEntityInterface[]|Collection $categories
     *
     * @return $this
     */
    public function setCategories(array|Collection $categories): self;
}
