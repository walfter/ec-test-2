<?php

namespace App\Contracts\Services;

use App\Contracts\Entities\CategoryEntityInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface CategoryServiceInterface
{
    /**
     * @return Collection|CategoryEntityInterface[]|LengthAwarePaginator
     */
    public function list(): Collection|array|LengthAwarePaginator;

    /**
     * @param string $id
     *
     * @return CategoryEntityInterface
     * @throws ModelNotFoundException
     */
    public function getById(string $id): CategoryEntityInterface;

    /**
     * @param array $attributes
     *
     * @return CategoryEntityInterface
     */
    public function store(array $attributes): CategoryEntityInterface;

    /**
     * @param string $id
     * @param array $attributes
     *
     * @return CategoryEntityInterface
     */
    public function updateById(string $id, array $attributes): CategoryEntityInterface;

    /**
     * @param string $id
     *
     * @return void
     */
    public function deleteById(string $id): void;
}
