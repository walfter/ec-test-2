<?php

namespace App\Contracts\Services;

use App\Contracts\Entities\ProductEntityInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface ProductServiceInterface
{
    /**
     * @return Collection|ProductEntityInterface[]|LengthAwarePaginator
     */
    public function list(): Collection|array|LengthAwarePaginator;

    /**
     * @param string $id
     *
     * @return ProductEntityInterface
     * @throws ModelNotFoundException
     */
    public function getById(string $id): ProductEntityInterface;

    /**
     * @param array $attributes
     *
     * @return ProductEntityInterface
     */
    public function store(array $attributes): ProductEntityInterface;

    /**
     * @param string $id
     * @param array $attributes
     *
     * @return ProductEntityInterface
     * @throws ModelNotFoundException
     */
    public function updateById(string $id, array $attributes): ProductEntityInterface;

    /**
     * @param string $id
     *
     * @return void
     * @throws ModelNotFoundException
     */
    public function deleteById(string $id): void;
}
