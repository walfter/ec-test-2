<?php

namespace App\Contracts;

use Illuminate\Contracts\Database\Eloquent\Builder;

interface FilterInterface
{
    /**
     * @param Builder $builder
     * @param string $value
     *
     * @return Builder
     */
    public function filtered(Builder $builder, string $value): Builder;
}
