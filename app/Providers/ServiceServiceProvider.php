<?php

namespace App\Providers;

use App\Contracts\Services\CategoryServiceInterface;
use App\Contracts\Services\ProductServiceInterface;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    /** @var array $bindings Бинды сервисов */
    public array $bindings = [
        CategoryServiceInterface::class => CategoryService::class,
        ProductServiceInterface::class => ProductService::class,
    ];
}
