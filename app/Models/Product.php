<?php

namespace App\Models;

use App\Contracts\Entities\CategoryEntityInterface;
use App\Contracts\Entities\ProductEntityInterface;
use App\Filters\BetweenFilter;
use App\Filters\BooleanFilter;
use App\Filters\ILikeFilter;
use App\Filters\ProductHasCategoryByLikeTitleFilter;
use App\Filters\ProductHasCategoryFilter;
use App\Filters\WithTrashedFilter;
use App\Traits\Filterable;
use App\Traits\UUIDIDTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * App\Models\Product
 *
 * @property string $id
 * @property string $title
 * @property float $price
 * @property bool $is_published
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|Product onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Product withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product filtered()
 */
class Product extends Model implements ProductEntityInterface
{
    use HasFactory;
    use UUIDIDTrait;
    use SoftDeletes;
    use Filterable;

    public const MIN_CATEGORIES_COUNT = 2;
    public const MAX_CATEGORIES_COUNT = 10;

    protected $fillable = [
        'id',
        'title',
        'price',
        'is_published',
    ];

    protected $casts = [
        'price' => 'double',
        'is_published' => 'boolean',
    ];

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function isPublished(): bool
    {
        return $this->is_published;
    }

    /**
     * @inheritDoc
     */
    public function setId(string $id): ProductEntityInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): ProductEntityInterface
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPrice(float $price): ProductEntityInterface
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPublished(bool $isPublished = true): ProductEntityInterface
    {
        $this->is_published = $isPublished;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isDeleted(): bool
    {
        return !is_null($this->deleted_at);
    }

    /**
     * @inheritDoc
     */
    public function setDeleted(bool $deleted = true): ProductEntityInterface
    {
        $this->deleted_at = $deleted ? now() : null;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCategories(): Collection|array
    {
        return $this->categories;
    }

    /**
     * @inheritDoc
     */
    public function getCategoriesID(): array
    {
        return array_map(fn (CategoryEntityInterface $item) => $item->getId(), (array) $this->categories);
    }

    /**
     * @inheritDoc
     */
    public function setCategories(array|Collection $categories): ProductEntityInterface
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAllowedFilters(): array
    {
        return [
            'title' => new ILikeFilter('title'),
            'category_id' => new ProductHasCategoryFilter(),
            'category_title' => new ProductHasCategoryByLikeTitleFilter(),
            'price' => new BetweenFilter('price'),
            'published' => new BooleanFilter('is_published'),
            'with_trashed' => new WithTrashedFilter(),
        ];
    }
}
