<?php

namespace App\Filters;

use App\Contracts\FilterInterface;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ILikeFilter implements FilterInterface
{
    public function __construct(
        private string $column
    ){
    }

    /**
     * @inheritDoc
     */
    public function filtered(Builder $builder, string $value): Builder
    {
        return $builder->where($this->column, 'ILIKE', '%' . $value . '%');
    }
}
