<?php

namespace App\Filters;

use App\Contracts\FilterInterface;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ProductHasCategoryFilter implements FilterInterface
{

    /**
     * @inheritDoc
     */
    public function filtered(Builder $builder, string $value): Builder
    {
        $values = explode(',', $value);

        return $builder->whereHas('categories', static function (Builder $builder) use ($values) : Builder {
            return $builder->whereIn('id', $values);
        });
    }
}
