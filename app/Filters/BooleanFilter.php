<?php

namespace App\Filters;

use App\Contracts\FilterInterface;
use Illuminate\Contracts\Database\Eloquent\Builder;

class BooleanFilter implements FilterInterface
{
    public function __construct(
        private string $column
    ){
    }

    /**
     * @inheritDoc
     */
    public function filtered(Builder $builder, string $value): Builder
    {
        $value = [
            '0' => false,
            '1' => true,
            'true' => true,
            'false' => false,
        ][mb_strtolower($value)] ?? false;

        return $builder->where($this->column, $value);
    }
}
