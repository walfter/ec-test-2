<?php

namespace App\Filters;

use App\Contracts\FilterInterface;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ProductHasCategoryByLikeTitleFilter implements FilterInterface
{
    /**
     * @inheritDoc
     */
    public function filtered(Builder $builder, string $value): Builder
    {
        return $builder->whereHas('categories', static function (Builder $builder) use ($value) : Builder {
            return $builder->where('title', 'ILIKE', $value);
        });
    }
}
