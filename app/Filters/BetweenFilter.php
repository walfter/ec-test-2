<?php

namespace App\Filters;

use App\Contracts\FilterInterface;
use Illuminate\Contracts\Database\Eloquent\Builder;

class BetweenFilter implements FilterInterface
{
    public function __construct(
        private string $column,
        private string $separator = ':'
    ){
    }

    /**
     * @inheritDoc
     */
    public function filtered(Builder $builder, string $value): Builder
    {
        $values = explode($this->separator, $value);

        if (count($values) === 0) {
            return $builder;
        }

        if (count($values) === 2) {
            return $builder->whereBetween($this->column, $values);
        }

        return $builder->where($this->column, $values[0]);
    }
}
