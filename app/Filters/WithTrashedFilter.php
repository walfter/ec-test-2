<?php

namespace App\Filters;

use App\Contracts\FilterInterface;
use Illuminate\Contracts\Database\Eloquent\Builder;

class WithTrashedFilter implements FilterInterface
{

    /**
     * @inheritDoc
     */
    public function filtered(Builder $builder, string $value): Builder
    {
        $value = [
                '0' => false,
                '1' => true,
                'true' => true,
                'false' => false,
            ][mb_strtolower($value)] ?? false;

        if ($value) {
            $builder->withTrashed();
        }

        return $builder;
    }
}
