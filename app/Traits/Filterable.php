<?php

namespace App\Traits;

use App\Contracts\FilterInterface;
use App\Exceptions\FilterNotExistsException;
use Illuminate\Contracts\Database\Eloquent\Builder;

trait Filterable
{
    /**
     * Список доступных фильтров
     *
     * @return array<int|string, string|FilterInterface>
     */
    protected function getAllowedFilters(): array
    {
        return $this->getFillable();
    }

    /**
     * @throws FilterNotExistsException
     */
    public function scopeFiltered(Builder $builder): Builder
    {
        $filters = request()->query('filter', []);
        $allowedFilters = $this->getAllowedFilters();

        foreach ($filters as $name => $value) {
            $this->checkFilter($name);
            $allowedFilter = $allowedFilters[$name] ?? $name;
            $builder = $this->handleFilter($builder, $allowedFilter, $value);
        }

        return $builder;
    }

    /**
     * @throws FilterNotExistsException
     */
    private function checkFilter(string $filter): void
    {
        $allowedFilters = $this->getAllowedFilters();
        if (isset($allowedFilters[$filter]) || in_array($filter, $allowedFilters, true)) {
            return;
        }

        throw new FilterNotExistsException($filter);
    }

    /**
     * @param Builder $builder
     * @param string|FilterInterface $filter
     * @param string $value
     *
     * @return Builder
     */
    private function handleFilter(Builder $builder, string|FilterInterface $filter, string $value): Builder
    {
        if ($filter instanceof FilterInterface) {
            return $filter->filtered($builder, $value);
        }

        return $builder->where($filter, $value);
    }
}
