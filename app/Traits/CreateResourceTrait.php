<?php

namespace App\Traits;

trait CreateResourceTrait
{
    /** @var int $status_code статус код для ответа */
    public static int $status_code = 200;

    /**
     * Создание ресурса с статус кодом 201
     *
     * @param $resource
     *
     * @return static
     */
    public static function created($resource): self
    {
        return new self($resource, 201);
    }

    /**
     * Перегрузка конструктора
     *
     * CreateResourceTrait constructor.
     * @param $resource
     * @param int $status_code
     */
    public function __construct($resource, int $status_code = 200)
    {
        parent::__construct($resource);
        static::$status_code = $status_code;
    }

    /**
     * Перегрузка метода который вызывается непосредственно перед рендером
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function toResponse($request)
    {
        return parent::toResponse($request)->setStatusCode(static::$status_code);
    }
}
