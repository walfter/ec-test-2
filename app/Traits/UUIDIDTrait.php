<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait UUIDIDTrait
{
    /**
     * Отключаем инкрементацию
     *
     * @return false
     */
    public function getIncrementing(): bool
    {
        return false;
    }

    /**
     * Определяем тип идентификатора модели
     *
     * @return string
     */
    public function getKeyType(): string
    {
        return 'string';
    }

    /**
     * Определяем загрузчик трейта
     */
    public static function bootUUIDIDTrait(): void
    {
        static::creating(function (Model $model) {
            if (is_null($model->attributesToArray()[$model->getKeyName()] ?? null)) {
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            }
        });
    }
}
