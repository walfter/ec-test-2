<?php

namespace App\Http\Requests\Api\V1\Product;

use App\Models\Product;
use App\Rules\Api\V1\Product\ProductCategoryRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $product_id = $this->route('id');
        return [
            'title' => [
                'nullable',
                Rule::unique(Product::class)->ignore($product_id),
            ],
            'price' => [
                'numeric',
                'nullable',
                'gte:0',
            ],
            'is_published' => [
                'bool',
                'nullable',
            ],
            'categories' => [
                'sometimes',
                'array',
                new ProductCategoryRule(),
                'min:' . Product::MIN_CATEGORIES_COUNT,
                'max:' . Product::MAX_CATEGORIES_COUNT,
            ],
        ];
    }
}
