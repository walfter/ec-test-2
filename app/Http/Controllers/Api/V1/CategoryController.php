<?php

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Services\CategoryServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Category\CategoryCreateRequest;
use App\Http\Requests\Api\V1\Category\CategoryUpdateRequest;
use App\Http\Resources\Api\V1\Category\CategoryListResource;
use App\Http\Resources\Api\V1\Category\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    public function __construct(
        private CategoryServiceInterface $service
    ){
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResource
     */
    public function index(): JsonResource
    {
        $categories = $this->service->list();

        return CategoryListResource::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryCreateRequest $request
     *
     * @return CategoryResource
     */
    public function store(CategoryCreateRequest $request): CategoryResource
    {
        $validated = $request->validated();
        $category = $this->service->store($validated);

        return CategoryResource::created($category);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return CategoryResource
     */
    public function show(string $id): CategoryResource
    {
        $category = $this->service->getById($id);

        return CategoryResource::make($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateRequest $request
     * @param string $id
     *
     * @return CategoryResource
     */
    public function update(CategoryUpdateRequest $request, string $id): CategoryResource
    {
        $validated = $request->validated();

        $category = $this->service->updateById($id, $validated);

        return CategoryResource::make($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     *
     * @return Response
     */
    public function destroy(string $id): Response
    {
        $this->service->deleteById($id);

        return \response()->noContent();
    }
}
