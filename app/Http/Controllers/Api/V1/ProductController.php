<?php

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Services\ProductServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Product\ProductCreateRequest;
use App\Http\Requests\Api\V1\Product\ProductUpdateRequest;
use App\Http\Resources\Api\V1\Product\ProductListResource;
use App\Http\Resources\Api\V1\Product\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    public function __construct(
        private ProductServiceInterface $service
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResource
     */
    public function index(): JsonResource
    {
        $products = $this->service->list();

        return ProductListResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductCreateRequest $request
     *
     * @return ProductResource
     */
    public function store(ProductCreateRequest $request): ProductResource
    {
        $validated = $request->validated();
        $product = $this->service->store($validated);

        return ProductResource::created($product);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return ProductResource
     */
    public function show(string $id): ProductResource
    {
        $product = $this->service->getById($id);

        return ProductResource::make($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param string $id
     *
     * @return ProductResource
     */
    public function update(ProductUpdateRequest $request, string $id): ProductResource
    {
        $attributes = $request->validated();
        $product = $this->service->updateById($id, $attributes);

        return ProductResource::make($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     *
     * @return Response
     */
    public function destroy(string $id): Response
    {
        $this->service->deleteById($id);

        return \response()->noContent();
    }
}
