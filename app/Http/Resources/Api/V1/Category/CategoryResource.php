<?php

namespace App\Http\Resources\Api\V1\Category;

use App\Contracts\Entities\CategoryEntityInterface;
use App\Traits\CreateResourceTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin CategoryEntityInterface
 */
class CategoryResource extends JsonResource
{
    use CreateResourceTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
        ];
    }
}
