<?php

namespace App\Http\Resources\Api\V1\Product;

use App\Contracts\Entities\ProductEntityInterface;
use App\Traits\CreateResourceTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ProductEntityInterface
 */
class ProductResource extends JsonResource
{
    use CreateResourceTrait;
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'price' => $this->getPrice(),
            'is_published' => $this->isPublished(),
            'is_deleted' => $this->isDeleted(),
            'categories' => ProductCategoryResource::collection($this->getCategories()),
        ];
    }
}
