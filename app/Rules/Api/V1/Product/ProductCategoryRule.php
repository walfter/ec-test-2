<?php

namespace App\Rules\Api\V1\Product;

use App\Models\Category;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class ProductCategoryRule implements Rule
{
    private array $ids = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ids = array_unique($value);
        $ids = array_filter($ids, fn (string $id) => Str::isUuid($id));
        $category_ids = Category::select('id')
            ->whereIn('id', $ids)
            ->pluck('id')
            ->toArray();
        $diff = array_diff($ids, $category_ids);
        $this->ids = $diff;
        return count($diff) === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.' . self::class, ['ids' => implode('; ', $this->ids)]);
    }
}
