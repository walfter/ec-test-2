<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FilterNotExistsException extends Exception
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        if (!$request->wantsJson()) {
            abort(400);
        }

        return response()->json([
            'message' => trans('exception.' . self::class, ['filter' => $this->message])
        ], 400);
    }
}
