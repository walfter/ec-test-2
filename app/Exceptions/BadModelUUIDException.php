<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use \Illuminate\Http\Request;
use Throwable;

class BadModelUUIDException extends Exception
{
    public function __construct(string $message = "", int $code = 400, ?Throwable $previous = null)
    {
        if (empty($message)) {
            $message = trans('exception.' . self::class);
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        if (!$request->wantsJson()) {
            abort($this->code);
        }

        return response()->json(['message' => $this->message], $this->code);
    }
}
