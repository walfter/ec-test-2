<?php

namespace App\Exceptions;

use App\Models\Product;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FewProductCategoriesException extends Exception
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        if (!$request->wantsJson()) {
            abort(400);
        }

        return response()->json([
            'message' => trans('exception.' . self::class, ['min' => Product::MIN_CATEGORIES_COUNT]),
        ], 400);
    }
}
