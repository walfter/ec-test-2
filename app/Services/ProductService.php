<?php

namespace App\Services;

use App\Contracts\Entities\CategoryEntityInterface;
use App\Contracts\Entities\ProductEntityInterface;
use App\Contracts\Repositories\ProductRepositoryInterface;
use App\Contracts\Services\ProductServiceInterface;
use App\Exceptions\BadModelUUIDException;
use App\Exceptions\FewProductCategoriesException;
use App\Exceptions\ManyProductCategoriesException;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ProductService implements ProductServiceInterface
{
    public function __construct(
        private ProductRepositoryInterface $repository
    ){
    }

    /**
     * @inheritDoc
     */
    public function list(): Collection|array|LengthAwarePaginator
    {
        return $this->repository->list();
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function getById(string $id): ProductEntityInterface
    {
        if (!Str::isUuid($id)) {
            throw new BadModelUUIDException();
        }

        return $this->repository->getById($id);
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function store(array $attributes): ProductEntityInterface
    {
        if (!Str::isUuid($attributes['id'])) {
            throw new BadModelUUIDException(trans('validation.uuid', ['attribute' => 'id']));
        }
        $categories = $this->getCategoriesFromAttributes($attributes);
        $entity = new Product();
        $entity->setId($attributes['id'])
            ->setTitle($attributes['title'])
            ->setPrice($attributes['price'])
            ->setPublished($attributes['is_published'] ?? true)
            ->setCategories($categories);

        return $this->repository->store($entity);
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function updateById(string $id, array $attributes): ProductEntityInterface
    {
        if (!Str::isUuid($id)) {
            throw new BadModelUUIDException();
        }
        $entity = new Product();
        $entity->setId($id)
            ->setTitle((string) $attributes['title'])
            ->setPrice((float) $attributes['price'])
            ->setPublished($attributes['is_published'] ?? true);

        if (isset($attributes['categories'])) {
            $categories = $this->getCategoriesFromAttributes($attributes);
            $entity->setCategories($categories);
        }

        return $this->repository->update($entity);
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function deleteById(string $id): void
    {
        if (!Str::isUuid($id)) {
            throw new BadModelUUIDException();
        }

        $this->repository->deleteById($id);
    }

    /**
     * @param CategoryEntityInterface[] $attributes
     *
     * @return array
     * @throws ManyProductCategoriesException
     * @throws FewProductCategoriesException
     */
    private function getCategoriesFromAttributes(array $attributes): array
    {
        $categories = [];
        foreach ($attributes['categories'] ?? [] as $category_id) {
            $categories[] = (new Category())
                ->setId($category_id);
        }

        if (count($categories) > Product::MAX_CATEGORIES_COUNT) {
            throw new ManyProductCategoriesException();
        }

        if (count($categories) < Product::MIN_CATEGORIES_COUNT) {
            throw new FewProductCategoriesException();
        }

        return $categories;
    }
}
