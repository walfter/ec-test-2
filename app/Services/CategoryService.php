<?php

namespace App\Services;

use App\Contracts\Entities\CategoryEntityInterface;
use App\Contracts\Repositories\CategoryRepositoryInterface;
use App\Contracts\Services\CategoryServiceInterface;
use App\Exceptions\BadModelUUIDException;
use App\Exceptions\DeleteCategoryWithProductException;
use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class CategoryService implements CategoryServiceInterface
{
    public function __construct(
        private CategoryRepositoryInterface $repository
    ){
    }

    /**
     * @inheritDoc
     */
    public function list(): Collection|array|LengthAwarePaginator
    {
        return $this->repository->list();
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function getById(string $id): CategoryEntityInterface
    {
        if (!Str::isUuid($id)) {
            throw new BadModelUUIDException();
        }

        return $this->repository->getById($id);
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function store(array $attributes): CategoryEntityInterface
    {
        if (!Str::isUuid($attributes['id'])) {
            throw new BadModelUUIDException(trans('validation.uuid', ['attribute' => 'id']));
        }

        $entity = new Category();
        $entity->setId($attributes['id'])
            ->setTitle($attributes['title']);

        return $this->repository->store($entity);
    }

    /**
     * @inheritDoc
     * @throws BadModelUUIDException
     */
    public function updateById(string $id, array $attributes): CategoryEntityInterface
    {
        if (!Str::isUuid($id)) {
            throw new BadModelUUIDException();
        }

        $entity = new Category();
        $entity->setId($id)
            ->setTitle($attributes['title']);

        return $this->repository->update($entity);
    }

    /**
     * @inheritDoc
     * @throws DeleteCategoryWithProductException
     */
    public function deleteById(string $id): void
    {
        if ($this->repository->productCountsById($id) > 0) {
            throw new DeleteCategoryWithProductException();
        }
        $this->repository->deleteById($id);
    }
}
