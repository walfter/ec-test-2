<?php

namespace Tests\Feature;

use App\Models\Category;
use Illuminate\Support\Str;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    private const BASE_URL = 'api/v1/categories/';

    public function test_store()
    {
        $payload = [
            'id' => Str::uuid(),
            'title' => Str::random(),
        ];

        $this->json('post', self::BASE_URL, $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                ],
            ]);
    }

    public function test_list()
    {
        $this->json('get', self::BASE_URL)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                    ]
                ],
            ]);
    }

    public function test_show()
    {
        $id = Category::latest()->first()->id;
        $this->json('get', self::BASE_URL . $id)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                ],
            ]);
    }

    public function test_update()
    {
        $payload = [
            'title' => Str::random(),
        ];

        $id = Category::latest()->first()->id;
        $this->json('patch', self::BASE_URL . $id, $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                ],
            ]);
    }

    public function test_delete_with_product()
    {
        $id = Category::has('products')->first()->id;
        $this->json('delete', self::BASE_URL . $id)
            ->assertStatus(400);
    }

    public function test_delete()
    {
        $id = Category::latest()->first()->id;
        $this->json('delete', self::BASE_URL . $id)
            ->assertNoContent();
    }
}
