<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Str;

class ProductControllerTest extends \Tests\TestCase
{
    public function test_store()
    {
        $payload = [
            'id' => Str::uuid(),
            'title' => Str::random(),
            'is_published' => true,
            'price' => rand(1, 10000),
            'categories' => $this->getCategoryIds(),
        ];

        $this->json('post', 'api/v1/products', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'price',
                    'title',
                    'is_published',
                    'is_deleted',
                    'categories' => [
                        '*' => [
                            'id',
                            'title',
                        ]
                    ],
                ],
            ]);
    }

    public function test_list()
    {
        $this->json('get', 'api/v1/products')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_title_filter()
    {
        $title = Product::select('title')
            ->pluck('title')
            ->shuffle()
            ->first();
        $this->json('get', 'api/v1/products?filter[title]=' . $title)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_category_title_filter()
    {
        $title = Category::has('products')->latest()->first()->title;
        $this->json('get', 'api/v1/products?filter[category_title]=' . $title)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_category_id_filter()
    {
        $id = Category::has('products')->latest()->first()->id;
        $this->json('get', 'api/v1/products?filter[category_id]=' . $id)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_price_filter()
    {
        $prices = Product::selectRaw('MIN(price) as min, MAX(price) as max')->first();
        $start = rand($prices->min, $prices->max);
        $end = rand($prices->min, $prices->max);
        $values = [
            $start,
            $end,
        ];

        sort($values);
        $this->json('get', 'api/v1/products?filter[price]=' . implode(':', $values))
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_published_filter()
    {
        $this->json('get', 'api/v1/products?filter[published]=true')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_with_trashed_filter()
    {
        $this->json('get', 'api/v1/products?filter[with_trashed]=true')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'price',
                        'title',
                        'is_published',
                        'is_deleted',
                        'categories' => [
                            '*' => [
                                'id',
                                'title',
                            ]
                        ],
                    ]
                ],
            ]);
    }

    public function test_show()
    {
        $id = Product::latest()->first()->id;
        $this->json('get', 'api/v1/products/' . $id)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'price',
                    'title',
                    'is_published',
                    'is_deleted',
                    'categories' => [
                        '*' => [
                            'id',
                            'title',
                        ]
                    ],
                ],
            ]);
    }

    public function test_update()
    {
        $payload = [
            'title' => Str::random(),
            'is_published' => true,
            'price' => rand(1, 10000),
            'categories' => $this->getCategoryIds(),
        ];

        $id = Product::latest()->first()->id;
        $this->json('patch', 'api/v1/products/' . $id, $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'price',
                    'title',
                    'is_published',
                    'is_deleted',
                    'categories' => [
                        '*' => [
                            'id',
                            'title',
                        ]
                    ],
                ],
            ]);
    }

    public function test_delete()
    {
        $id = Product::latest()->first()->id;
        $this->json('delete', 'api/v1/products/' . $id)
            ->assertNoContent();
    }

    /**
     * @return array
     */
    private function getCategoryIds(): array
    {
        $rand = rand(Product::MIN_CATEGORIES_COUNT, Product::MAX_CATEGORIES_COUNT) - 1;

        return Category::select('id')
            ->pluck('id')
            ->shuffle()
            ->splice(1, $rand)
            ->toArray();
    }
}
