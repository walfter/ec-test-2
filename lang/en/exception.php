<?php

return [
    \App\Exceptions\BadModelUUIDException::class => 'The path ID must be a valid UUID.',
    \App\Exceptions\ManyProductCategoriesException::class => 'Max product categories :max',
    \App\Exceptions\FewProductCategoriesException::class => 'Min product categories :min',
    \App\Exceptions\DeleteCategoryWithProductException::class => 'You can\'t delete category with products',
    \App\Exceptions\FilterNotExistsException::class => 'Filter :filter not allowed',
];
